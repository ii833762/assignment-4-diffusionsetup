# Introduction to Numerical Modelling
# Assignment 4 - Setting up the diffusion equation
# Defines functions for use in DiffusionSolve

import numpy as np
import numpy.linalg as la          # linear algebra package


def FTCS_fixed_zerograd(phi, K, Q, dx, nt, dt):
    """One-dimensional diffusion of some parameter phi in dimension x,
        using forward-time centred-space method.
        Uses the thermal diffusivity coefficient K, rate of heating Q,
        resolution dx, and runs for nt time steps, each of length dt.
        Boundary conditions are fixed at the start
        and zero-gradient at the end."""
    
    nx = len(phi)
    
    # Dimensionless diffusion coefficient
    d = dt*K/dx**2    

    # New time-step array for phi
    phinew = phi.copy()
    
    # FTCS for all time steps
    for i in range(nt):
        # loop over all internal points
        for i in range (1,nx-1):
            phinew[i] = phi[i] + d*(phi[i+1] - 2*phi[i] + phi[i-1]) + Q*dt
        # Boundary conditions at the start
        phinew[0] = phi[0]
        # Boundary conditions at the end
        phinew[nx-1] = phinew[nx-2]
        # Update phi for the next time-step
        phi = phinew.copy()
    
    return phi


def BTCS_fixed_zerograd(phi, K, Q, dx, nt, dt):
    """One-dimensional diffusion of some parameter phi in dimension x,
        using backward-time centred-space method.
        Uses the thermal diffusivity coefficient K, rate of heating Q,
        resolution dx, and runs for nt time steps, each of length dt.
        Boundary conditions are fixed at the start
        and zero-gradient at the end."""

    # Dimensionless diffusion coefficient
    d = dt*K/dx**2    
    
    # Create a matrix representing BTCS
    nx = len(phi)
    M = np.zeros([nx,nx])
    
    # Boundary conditions at the start - fixed
    M[0,0] = 1
    
    # Boundary conditions at the end - zero gradient
    M[-1,-1] = 1
    M[-1,-2] = -1
    
    # Other elements in the matrix
    for i in range(1,nx-1):
        M[i,i-1] = -d
        M[i,i] = 1 + 2*d
        M[i,i+1] = -d
    
    # BTCS for all time steps
    for i in range(nt):
        # RHS vector
        RHS = phi + Q*dt
        # RHS vector for boundary conditions at the start
        RHS[0] = phi[0]
        # RHS for boundary conditions at the end
        RHS[-1] = 0  
        # Solve matrix equation to update phi
        phi = la.solve(M,RHS)
    
    return phi
